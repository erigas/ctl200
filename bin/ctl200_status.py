#!/usr/bin/env python
# coding=utf-8
"""
Test Laser

"""
from __future__ import print_function
import argparse
from time import sleep, time
from datetime import date, datetime
from ctl200 import laser

__all__ = ['laser.Laser']
__author__ = "Evangelos Rigas"
__copyright__ = "Copyright 2019, Evangelos Rigas"
__credits__ = ["Evangelos Rigas"]
__license__ = "GPL v3"
__version__ = "0.9.0-$Format:%h$"
__maintainer__ = "Evangelos Rigas"
__email__ = "e.rigas@cranfield.ac.uk"
__status__ = "Development"

cp_short = "CTL-200 driver test, v0.9.0\nCopyright (C) 2019 Evangelos Rigas.\n"
cp = """CTL-200 I-V curves, v0.9.0
Copyright (C) 2019 Evangelos Rigas.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law."""

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description='CTL-200 driver status, v0.9.0', epilog=cp)
parser.add_argument("-p", "--port", type=str, default='/dev/ttyUSB0', help="Serial port (default: /dev/ttyUSB0).")
parser.add_argument("-c", "--copyright", action='store_true', help="Display copyright.")

if __name__ == "__main__":
    args = parser.parse_args()

    if args.copyright:
        print(cp)
        exit(0)

    print("CTL-200 driver status, v0.9.0\nCopyright (C) 2019 Evangelos Rigas.\n")
    with laser.Laser(args.port) as las:
        print()
        las.temp_setpoint = 20
        las.laser_current = 0
        print('Firmware version:   v{:>3}'.format(las.version))
        print('Laser status:        {:>1}'.format(las.laser_status))
        print('Laser current:   {:8.2f}       mA'.format(las.laser_current))
        print('TEC voltage:     {: 10.4f}     V'.format(las.tec_voltage))
        print('TEC current:     {: 10.4f}     A'.format(las.tec_current))
        print('PD current:         {: 4.2e}   mA'.format(las.photo_current))
        print('Thermistor:    {: 12.4f}     Ohms'.format(las.rt_resistance))
        print('Temperature:     {: 8.2f}       C'.format(las.laser_temp))
        las.laser_current = 0
        las.laser_status = 0
    exit(0)
