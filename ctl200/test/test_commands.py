#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
module for Testing the commands module

"""
from __future__ import print_function, division

import sys
from unittest import TestCase

import ctl200.comm as comm
from ctl200.commands import *

if sys.version_info > (3, 3):
    import unittest.mock as mock
else:
    import mock

__author__ = "Evangelos Rigas"
__copyright__ = "Copyright 2019, Evangelos Rigas"
__credits__ = ["Evangelos Rigas"]
__license__ = "GPL v3"
__version__ = "0.9.0-$Format:%h$"
__maintainer__ = "Evangelos Rigas"
__email__ = "e.rigas@cranfield.ac.uk"
__status__ = "Development"


class TestParseResponse(TestCase):
    resp = [None,
            'version\r\nV0.7\r\n>>',
            'version\r\nasdf\r\n>>']
    res = [-1, '0.7', -1]
    response = '\r\nV{}\r\n>>'

    def test_parse_None(self):
        r = parse_response(self.response, self.resp[0])
        self.assertEqual(r, self.res[0])

    def test_parse_valid(self):
        r = parse_response(self.response, self.resp[1])
        self.assertEqual(r.fixed[0], self.res[1])

    def test_parse_garbage_(self):
        r = parse_response(self.response, self.resp[2])
        self.assertEqual(r, self.res[2])


class TestVersion(TestCase):
    resp = ['version\r\nV0.7\r\n>>',
            'version\r\nasdf\r\n>>']
    res = ['0.7', -1]
    testfun = Version()

    def test_parse_valid(self):
        r = self.testfun.parse(self.resp[0])
        self.assertEqual(r, self.res[0])

    def test_parse_garbage(self):
        r = self.testfun.parse(self.resp[1])
        self.assertEqual(r, self.res[1])


class TestLaserStatus(TestCase):
    resp = ['lason\r\n0\r\n>>',
            'lason\r\n1\r\n>>',
            'lason\r\nasd\r\n>>']
    res = [0, 1, -1]
    testfun = LaserStatus()

    def test_parse_valid_0(self):
        r = self.testfun.parse(self.resp[0])
        self.assertEqual(r, self.res[0])

    def test_parse_valid_1(self):
        r = self.testfun.parse(self.resp[1])
        self.assertEqual(r, self.res[1])

    def test_parse_garbage(self):
        r = self.testfun.parse(self.resp[2])
        self.assertEqual(r, self.res[2])


class TestLaserOn(TestCase):
    resp = ['lason\r\n0\r\n>>',
            'lason\r\n1\r\n>>',
            'lason\r\nasd\r\n>>']
    res = [0, 1, -1]
    testfun = LaserStatus()

    def test_parse_valid_0(self):
        r = self.testfun.parse(self.resp[0])
        self.assertEqual(r, self.res[0])

    def test_parse_valid_1(self):
        r = self.testfun.parse(self.resp[1])
        self.assertEqual(r, self.res[1])

    def test_parse_garbage(self):
        r = self.testfun.parse(self.resp[2])
        self.assertEqual(r, self.res[2])

    def test_check_arguments_valid_0(self):
        self.assertTrue(self.testfun.check_arguments(0))

    def test_check_arguments_valid_1(self):
        self.assertTrue(self.testfun.check_arguments(1))

    def test_check_arguments_negative(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, -100)

    def test_check_arguments_invalid_str(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, '1')

    def test_check_arguments_greater(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, 100)


class TestLaserCurrent(TestCase):
    resp = ['ilaser\r\n80.0\r\n>>',
            'ilaser\r\nasd\r\n>>']
    res = [80.0, -1]
    testfun = LaserCurrent()

    def test_parse_valid_float(self):
        r = self.testfun.parse(self.resp[0])
        self.assertEqual(r, self.res[0])

    def test_parse_garbage(self):
        r = self.testfun.parse(self.resp[1])
        self.assertEqual(r, self.res[1])

    def test_check_arguments_valid_float(self):
        self.assertTrue(self.testfun.check_arguments(80.0))

    def test_check_arguments_valid_int(self):
        self.assertTrue(self.testfun.check_arguments(10))

    def test_check_arguments_negative(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, -100)

    def test_check_arguments_invalid_str(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, '10')

    def test_check_arguments_greater(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, 150)


class TestTECVoltageMin(TestCase):
    resp = ['vtmin\r\n-1.0\r\n>>',
            'vtmin\r\nasdf\r\n>>']
    res = [-1.0, -1]
    testfun = TECVoltageMin()

    def test_parse_valid_float(self):
        r = self.testfun.parse(self.resp[0])
        self.assertEqual(r, self.res[0])

    def test_parse_garbage(self):
        r = self.testfun.parse(self.resp[1])
        self.assertEqual(r, self.res[1])

    def test_check_arguments_valid_float(self):
        self.assertTrue(self.testfun.check_arguments(-1.5))

    def test_check_arguments_valid_int(self):
        self.assertTrue(self.testfun.check_arguments(-2))

    def test_check_arguments_positive(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, 1)

    def test_check_arguments_invalid_str(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, '10')

    def test_check_arguments_lower(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, -3)


class TestTECVoltageMax(TestCase):
    resp = ['vtmin\r\n1.0\r\n>>',
            'vtmin\r\nasdf\r\n>>']
    res = [1.0, -1]
    testfun = TECVoltageMax()

    def test_parse_valid_float(self):
        r = self.testfun.parse(self.resp[0])
        self.assertEqual(r, self.res[0])

    def test_parse_garbage(self):
        r = self.testfun.parse(self.resp[1])
        self.assertEqual(r, self.res[1])

    def test_check_arguments_valid_float(self):
        self.assertTrue(self.testfun.check_arguments(1.5))

    def test_check_arguments_valid_int(self):
        self.assertTrue(self.testfun.check_arguments(2))

    def test_check_arguments_negative(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, -1)

    def test_check_arguments_invalid_str(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, '10')

    def test_check_arguments_greater(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, 3)


class TestTempSetpoint(TestCase):
    resp = ['tset\r\n21.0\r\n>>',
            'vtmin\r\nasdf\r\n>>']
    res = [21.0, -1]
    testfun = TempSetpoint()

    def test_parse_valid_float(self):
        r = self.testfun.parse(self.resp[0])
        self.assertEqual(r, self.res[0])

    def test_parse_garbage(self):
        r = self.testfun.parse(self.resp[1])
        self.assertEqual(r, self.res[1])

    def test_check_arguments_valid_float(self):
        self.assertTrue(self.testfun.check_arguments(25.5))

    def test_check_arguments_valid_int(self):
        self.assertTrue(self.testfun.check_arguments(30))

    def test_check_arguments_negative(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, -1)

    def test_check_arguments_invalid_str(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, '10')

    def test_check_arguments_greater(self):
        self.assertRaises(AssertionError, self.testfun.check_arguments, 38)


class TestStatusQuery(TestCase):
    resp = ['rtact\r\n10000.5\r\n>>',
            'rtact\r\n2.0\r\n>>',
            'rtact\r\nasdf\r\n>>']
    res = [10000.5, 2.0, -1]
    testfun = StatusQuery()

    def test_parse_valid_float(self):
        r = self.testfun.parse(self.resp[0])
        self.assertEqual(r, self.res[0])

    def test_parse_valid_int(self):
        r = self.testfun.parse(self.resp[1])
        self.assertEqual(r, self.res[1])

    def test_parse_garbage(self):
        r = self.testfun.parse(self.resp[2])
        self.assertEqual(r, self.res[2])


class Testexecute(TestCase):
    @staticmethod
    def mock_query_Version(*_):
        return 'version\r\nV0.7\r\n>>'

    @staticmethod
    def mock_query_LaserStatus(*_):
        return 'lason\r\n0\r\n>>'

    @staticmethod
    def mock_send_LaserOn(*_):
        return 'lason\r\n1\r\n>>'

    @staticmethod
    def mock_send_LaserCurrent(*_):
        return 'ilaser\r\n10.0\r\n>>'

    com = comm.CommLayer()
    executer = Execute(com)

    @mock.patch('ctl200.comm.CommLayer.query', mock_query_Version)
    def test__call___Version(self):
        r = self.executer(Version)
        self.assertEqual(r, '0.7')

    @mock.patch('ctl200.comm.CommLayer.query', mock_query_LaserStatus)
    def test__call___LaserStatus(self):
        r = self.executer(LaserStatus)
        self.assertEqual(r, 0)

    @mock.patch('ctl200.comm.CommLayer.send', mock_send_LaserOn)
    def test__call___LaserOn(self):
        r = self.executer(LaserOn, 1)
        self.assertEqual(r, 1)

    @mock.patch('ctl200.comm.CommLayer.send', mock_send_LaserCurrent)
    def test__call___LaserCurrent_valid(self):
        r = self.executer(LaserCurrent, 10.0)
        self.assertEqual(r, 10.0)

    @mock.patch('ctl200.comm.CommLayer.send', mock_send_LaserCurrent)
    def test__call___LaserCurrent_OutOfRange(self):
        self.assertRaises(AssertionError, self.executer, LaserCurrent, 150)
