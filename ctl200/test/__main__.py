#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Main for the test package

"""

import unittest

if __name__ == '__main__':
    test_suite = unittest.TestLoader().discover('.')
    unittest.TextTestRunner(verbosity=1).run(test_suite)
